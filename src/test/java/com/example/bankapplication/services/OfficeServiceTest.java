package com.example.bankapplication.services;

import com.example.bankapplication.BankApplicationTests;
import com.example.bankapplication.forms.ManagerForm;
import com.example.bankapplication.models.Manager;
import com.example.bankapplication.models.Office;
import com.example.bankapplication.repositories.ManagersRepository;
import com.example.bankapplication.repositories.OfficeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class OfficeServiceTest extends BankApplicationTests {
    @Autowired
    private OfficeService officeService;

    @Autowired
    private ManagersRepository managersRepository;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private OfficeRepository officeRepository;

    Office testOffice = Office.builder()
            .id(1)
            .name("Test office")
            .address("Test street")
            .phoneNumber("88001234567")
            .weekendWork('n')
            .build();

    ManagerForm testManager = ManagerForm.builder()
            .name("Test manager")
            .dateOfBirth("07-07-1988")
            .email("test@bank.com")
            .seniority(0)
            .build();

    @Test
    void testGetAllOffices() {
        List<Office> offices = officeService.getAllOffices();
        assertNotNull(offices);
    }

    @Test
    void testFindByAddressContainingIgnoreCase() {
        String address = "";
        String findAddress = String.valueOf(officeRepository.findByAddressContainingIgnoreCase(address));
        assertNotNull(findAddress);
    }

    @Test
    void testGetByNonExistingId() {
        Integer testId = 100;

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> officeService.getOffice(testId),
                format("Expected to return nothing and throw exception," +
                        "since object with generated id %s doesn't exist", testId)
        );

        assertEquals(format("404 NOT_FOUND \"Office with id %s not found\"", testId),
                exception.getMessage());
    }

    @Test
    void testGetAllManagersByOffice() {
        List<Manager> managersByOffice = managersRepository.findAllByWorkplace_Id(testOffice.getId());
        assertNotNull(managersByOffice);
    }

    @Test
    void testGetAllManagersWithoutOffice() {
        List<Manager> managersWithoutOffice = managersRepository.findAllByWorkplaceIsNull();
        assertNotNull(managersWithoutOffice);
    }

    @Test
    void testGetOffice() {
        officeRepository.save(testOffice);
        Office createdOffice = officeService.getOffice(testOffice.getId());
        assertNotNull(createdOffice);

        assertEquals(1, createdOffice.getId());
        assertEquals("Test office", createdOffice.getName());
        assertEquals("Test street", createdOffice.getAddress());
        assertEquals("88001234567", createdOffice.getPhoneNumber());
        assertEquals('n', createdOffice.getWeekendWork());
    }

    @Test
    void testAddManagerToOffice() {
        Office office = officeRepository.save(testOffice);
        Manager manager = managerService.addManager(testManager);

        manager.setWorkplace(office);
        assertNotNull(manager);

        assertEquals(manager.getWorkplace(), office);
    }

    @Test
    void testUpdateOffice() {
        Office officeToUpdate = Office.builder()
                .id(testOffice.getId())
                .name("New est office")
                .address("New est street")
                .phoneNumber("88001111111")
                .weekendWork('y')
                .build();

        assertNotEquals(officeToUpdate.getName(), testOffice.getName());
        assertNotEquals(officeToUpdate.getAddress(), testOffice.getAddress());
        assertNotEquals(officeToUpdate.getPhoneNumber(), testOffice.getPhoneNumber());
        assertNotEquals(officeToUpdate.getWeekendWork(), testOffice.getWeekendWork());
    }
}
