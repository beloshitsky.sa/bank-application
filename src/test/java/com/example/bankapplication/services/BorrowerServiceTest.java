package com.example.bankapplication.services;

import com.example.bankapplication.BankApplicationTests;
import com.example.bankapplication.forms.BorrowerForm;
import com.example.bankapplication.models.Borrower;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.repositories.FeedbackRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class BorrowerServiceTest extends BankApplicationTests {

    @Autowired
    private BorrowerService borrowerService;
    @Autowired
    private FeedbackRepository feedbackRepository;

    BorrowerForm testBorrower = BorrowerForm.builder()
            .name("Test borrower")
            .dateOfBirth("01-01-2000")
            .phoneNumber("+79001234567")
            .sumOfCredit(1000)
            .term(12)
            .build();

    @Test
    void testAddBorrower() {
        Borrower createdBorrower = borrowerService.addBorrower(testBorrower);

        assertNotNull(createdBorrower.getId());
        assertEquals(testBorrower.getName(), createdBorrower.getName());
        assertEquals(testBorrower.getDateOfBirth(), createdBorrower.getDateOfBirth());
        assertEquals(testBorrower.getPhoneNumber(), createdBorrower.getPhoneNumber());
        assertEquals(testBorrower.getSumOfCredit(), createdBorrower.getSumOfCredit());
        assertEquals(testBorrower.getTerm(), createdBorrower.getTerm());
        assertEquals(testBorrower.getPayment(), createdBorrower.getPayment());
        assertEquals(testBorrower.getInterestRate(), createdBorrower.getInterestRate());
        assertEquals(testBorrower.getOverpayment(), createdBorrower.getOverpayment());
    }

    @Test
    void testGetAllBorrowers() {
        List<Borrower> borrowers = borrowerService.getAllBorrowers();
        assertNotNull(borrowers);
    }

    @Test
    void testDeleteBorrower() {
        Borrower createdBorrower = borrowerService.addBorrower(testBorrower);
        borrowerService.deleteBorrower(createdBorrower.getId());
        Integer createdBorrowerId = createdBorrower.getId();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> borrowerService.getBorrower(createdBorrowerId),
                format("Expected to return nothing and throw exception, since borrower with id %s was deleted",
                        createdBorrowerId));

        assertEquals(format("404 NOT_FOUND \"Borrower with id %s not found\"", createdBorrowerId),
                exception.getMessage());
    }

    @Test
    void testGetBorrower() {
        Borrower createdBorrower = borrowerService.addBorrower(testBorrower);
        Borrower borrower = borrowerService.getBorrower(createdBorrower.getId());

        assertNotNull(borrower);

        assertEquals(borrower.getName(), createdBorrower.getName());
        assertEquals(borrower.getDateOfBirth(), createdBorrower.getDateOfBirth());
        assertEquals(borrower.getPhoneNumber(), createdBorrower.getPhoneNumber());
        assertEquals(borrower.getSumOfCredit(), createdBorrower.getSumOfCredit());
        assertEquals(borrower.getTerm(), createdBorrower.getTerm());
        assertEquals(borrower.getPayment(), createdBorrower.getPayment());
        assertEquals(borrower.getInterestRate(), createdBorrower.getInterestRate());
        assertEquals(borrower.getOverpayment(), createdBorrower.getOverpayment());
    }

    @Test
    void testGetByNonExistingId() {
        Integer testId = 100;

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> borrowerService.getBorrower(testId),
                format("Expected to return nothing and throw exception," +
                        "since object with generated id %s doesn't exist", testId)
        );

        assertEquals(format("404 NOT_FOUND \"Borrower with id %s not found\"", testId),
                exception.getMessage());
    }

    @Test
    void testUpdateBorrower() {
        BorrowerForm testUpdateForm = BorrowerForm.builder()
                .name("New test borrower")
                .dateOfBirth("01-01-1999")
                .phoneNumber("+79001111111")
                .sumOfCredit(500001)
                .term(15)
                .build();

        Borrower createdBorrower = borrowerService.addBorrower(testBorrower);
        Borrower updatedBorrower = borrowerService.updateBorrower(createdBorrower.getId(), testUpdateForm);

        assertEquals(updatedBorrower.getId(), createdBorrower.getId(),
                "IDs shouldn't be changed during the update");

        assertNotEquals(updatedBorrower.getName(), createdBorrower.getName());
        assertNotEquals(updatedBorrower.getDateOfBirth(), createdBorrower.getDateOfBirth());
        assertNotEquals(updatedBorrower.getPhoneNumber(), createdBorrower.getPhoneNumber());
        assertNotEquals(updatedBorrower.getSumOfCredit(), createdBorrower.getSumOfCredit());
        assertNotEquals(updatedBorrower.getTerm(), createdBorrower.getTerm());
        assertNotEquals(updatedBorrower.getPayment(), createdBorrower.getPayment());
        assertNotEquals(updatedBorrower.getInterestRate(), createdBorrower.getInterestRate());
        assertNotEquals(updatedBorrower.getOverpayment(), createdBorrower.getOverpayment());
    }

    @Test
    void TestGetFeedbacksByBorrower() {
        Borrower createdBorrower = borrowerService.addBorrower(testBorrower);
        List<Feedback> testFeedbacks = feedbackRepository.
                findAllByBorrowerAuthor_Id(createdBorrower.getId());
        assertEquals(0, testFeedbacks.size());
    }
}
