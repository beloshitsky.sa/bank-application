package com.example.bankapplication.services;

import com.example.bankapplication.BankApplicationTests;
import com.example.bankapplication.forms.FeedbackForm;
import com.example.bankapplication.models.Feedback;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FeedbackServiceTest extends BankApplicationTests {

    @Autowired
    private FeedbackService feedbackService;

    FeedbackForm testFeedback = FeedbackForm.builder()
            .content("Ok")
            .rate(5)
            .authorName("Anonymous")
            .build();

    @Test
    void addFeedback() {
        Feedback createdFeedback = feedbackService.addFeedback(testFeedback);

        assertNotNull(createdFeedback.getId());

        assertEquals(testFeedback.getContent(), createdFeedback.getContent());
        assertEquals(testFeedback.getRate(), createdFeedback.getRate());
        assertEquals(testFeedback.getAuthorName(), createdFeedback.getAuthorName());
    }

    @Test
    void getAllFeedbacks() {
        List<Feedback> feedbacks = feedbackService.getAllFeedbacks();
        assertNotNull(feedbacks);
    }
}
