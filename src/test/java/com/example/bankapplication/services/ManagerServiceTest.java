package com.example.bankapplication.services;

import com.example.bankapplication.BankApplicationTests;
import com.example.bankapplication.forms.ManagerForm;
import com.example.bankapplication.models.Manager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class ManagerServiceTest extends BankApplicationTests {

    @Autowired
    private ManagerService managerService;

    ManagerForm testManager = ManagerForm.builder()
            .name("Test manager")
            .dateOfBirth("07-07-1988")
            .email("test@bank.com")
            .seniority(0)
            .build();

    @Test
    void testAddManager() {
        Manager createdManager = managerService.addManager(testManager);

        assertNotNull(createdManager.getId());

        assertEquals(testManager.getName(), createdManager.getName());
        assertEquals(testManager.getDateOfBirth(), createdManager.getDateOfBirth());
        assertEquals(testManager.getEmail(), createdManager.getEmail());
        assertEquals(testManager.getSeniority(), createdManager.getSeniority());
    }

    @Test
    void testGetAllManagers() {
        List<Manager> managers = managerService.getAllManagers();
        assertNotNull(managers);
    }

    @Test
    void testDeleteManager() {
        Manager createdManager = managerService.addManager(testManager);
        managerService.deleteManager(createdManager.getId());
        Integer createdManagerId = createdManager.getId();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> managerService.getManager(createdManagerId),
                format("Expected to return nothing and throw exception, since manager with id %s was deleted",
                        createdManagerId));

        assertEquals(format("404 NOT_FOUND \"Manager with id %s not found\"", createdManagerId),
                exception.getMessage());

    }

    @Test
    void testGetManager() {
        Manager createdManager = managerService.addManager(testManager);
        Manager manager = managerService.getManager(createdManager.getId());

        assertNotNull(manager);

        assertEquals(manager.getName(), createdManager.getName());
        assertEquals(manager.getDateOfBirth(), createdManager.getDateOfBirth());
        assertEquals(manager.getEmail(), createdManager.getEmail());
        assertEquals(manager.getSeniority(), createdManager.getSeniority());
    }

    @Test
    void testGetByNonExistingId() {
        Integer testId = 100;

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> managerService.getManager(testId),
                format("Expected to return nothing and throw exception," +
                        "since object with generated id %s doesn't exist", testId)
        );

        assertEquals(format("404 NOT_FOUND \"Manager with id %s not found\"", testId),
                exception.getMessage());
    }

    @Test
    void testUpdateManager() {
        ManagerForm testUpdateForm = ManagerForm.builder()
                .name("New test manager")
                .dateOfBirth("07-07-1990")
                .email("newTest@bank.com")
                .seniority(5)
                .build();

        Manager createdManager = managerService.addManager(testManager);
        Manager updatedManager = managerService.updateManager(testUpdateForm, createdManager.getId());

        assertEquals(updatedManager.getId(), createdManager.getId(),
                "IDs shouldn't be changed during the update");

        assertNotEquals(updatedManager.getName(), createdManager.getName());
        assertNotEquals(updatedManager.getDateOfBirth(), createdManager.getDateOfBirth());
        assertNotEquals(updatedManager.getEmail(), createdManager.getEmail());
        assertNotEquals(updatedManager.getSeniority(), createdManager.getSeniority());
    }
}
