package com.example.bankapplication.services;

import com.example.bankapplication.BankApplicationTests;
import com.example.bankapplication.forms.DepositorForm;
import com.example.bankapplication.models.Depositor;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.repositories.FeedbackRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.*;

class DepositorServiceTest extends BankApplicationTests {

    @Autowired
    private DepositorService depositorService;

    @Autowired
    private FeedbackRepository feedbackRepository;

    DepositorForm testDepositor = DepositorForm.builder()
            .name("Test depositor")
            .dateOfBirth("01-01-1991")
            .phoneNumber("+79001234567")
            .depositSum(1000)
            .term(12)
            .build();

    @Test
    void testAddDepositor() {
        Depositor createdDepositor = depositorService.addDepositor(testDepositor);

        assertNotNull(createdDepositor.getId());

        assertEquals(testDepositor.getName(), createdDepositor.getName());
        assertEquals(testDepositor.getDateOfBirth(), createdDepositor.getDateOfBirth());
        assertEquals(testDepositor.getPhoneNumber(), createdDepositor.getPhoneNumber());
        assertEquals(testDepositor.getDepositSum(), createdDepositor.getDepositSum());
        assertEquals(testDepositor.getTerm(), createdDepositor.getTerm());
        assertEquals(testDepositor.getInterestRate(), createdDepositor.getInterestRate());
        assertEquals(testDepositor.getDepositIncome(), createdDepositor.getDepositIncome());
    }

    @Test
    void testGetAllDepositors() {
        List<Depositor> depositors = depositorService.getAllDepositors();
        assertNotNull(depositors);
    }

    @Test
    void testDeleteDepositor() {
        Depositor createdDepositor = depositorService.addDepositor(testDepositor);
        depositorService.deleteDepositor(createdDepositor.getId());
        Integer createdDepositorId = createdDepositor.getId();

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> depositorService.getDepositor(createdDepositorId),
                format("Expected to return nothing and throw exception, since depositor with id %s was deleted",
                        createdDepositorId));

        assertEquals(format("404 NOT_FOUND \"Depositor with id %s not found\"", createdDepositorId),
                exception.getMessage());
    }

    @Test
    void testGetDepositor() {
        Depositor createdDepositor = depositorService.addDepositor(testDepositor);
        Depositor depositor = depositorService.getDepositor(createdDepositor.getId());

        assertNotNull(depositor);

        assertEquals(depositor.getName(), createdDepositor.getName());
        assertEquals(depositor.getDateOfBirth(), createdDepositor.getDateOfBirth());
        assertEquals(depositor.getPhoneNumber(), createdDepositor.getPhoneNumber());
        assertEquals(depositor.getDepositSum(), createdDepositor.getDepositSum());
        assertEquals(depositor.getTerm(), createdDepositor.getTerm());
        assertEquals(depositor.getInterestRate(), createdDepositor.getInterestRate());
        assertEquals(depositor.getDepositIncome(), createdDepositor.getDepositIncome());
    }

    @Test
    void testGetByNonExistingId() {
        Integer testId = 100;

        ResponseStatusException exception = assertThrows(
                ResponseStatusException.class,
                () -> depositorService.getDepositor(testId),
                format("Expected to return nothing and throw exception," +
                        "since object with generated id %s doesn't exist", testId)
        );

        assertEquals(format("404 NOT_FOUND \"Depositor with id %s not found\"", testId),
                exception.getMessage());
    }

    @Test
    void testUpdateDepositor() {
        DepositorForm testUpdateForm = DepositorForm.builder()
                .name("New test depositor")
                .dateOfBirth("01-01-1980")
                .phoneNumber("+79002345678")
                .depositSum(1000000)
                .term(9)
                .build();

        Depositor createdDepositor = depositorService.addDepositor(testDepositor);
        Depositor updatedDepositor = depositorService.updateDepositor(createdDepositor.getId(), testUpdateForm);

        assertEquals(updatedDepositor.getId(), createdDepositor.getId(),
                "IDs shouldn't be changed during the update");

        assertNotEquals(updatedDepositor.getName(), createdDepositor.getName());
        assertNotEquals(updatedDepositor.getDateOfBirth(), createdDepositor.getDateOfBirth());
        assertNotEquals(updatedDepositor.getPhoneNumber(), createdDepositor.getPhoneNumber());
        assertNotEquals(updatedDepositor.getDepositSum(), createdDepositor.getDepositSum());
        assertNotEquals(updatedDepositor.getTerm(), createdDepositor.getTerm());
        assertNotEquals(updatedDepositor.getInterestRate(), createdDepositor.getInterestRate());
        assertNotEquals(updatedDepositor.getDepositIncome(), createdDepositor.getDepositIncome());
    }

    @Test
    void testGetFeedbacksByDepositor() {
        Depositor createdDepositor = depositorService.addDepositor(testDepositor);
        List<Feedback> testFeedbacks = feedbackRepository.
                findAllByDepositorAuthor_id(createdDepositor.getId());
        assertEquals(0, testFeedbacks.size());
    }
}
