package com.example.bankapplication.controller;

import com.example.bankapplication.forms.BorrowerForm;
import com.example.bankapplication.models.Borrower;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.services.BorrowerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class BorrowerController {

    private final BorrowerService borrowerService;

    @GetMapping("/borrowers")
    public String getBorrowersPage(Model model) {
        List<Borrower> borrowers = borrowerService.getAllBorrowers();
        model.addAttribute("borrowers", borrowers);
        return "/borrowers";
    }

    @PostMapping("/borrowers")
    public String addBorrower(BorrowerForm form, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "Ошибки в данных!");
            return "redirect:/borrowers";
        }
        borrowerService.addBorrower(form);
        return "redirect:/borrowers";
    }

    @PostMapping("/borrowers/{id}/delete")
    public String deleteBorrower(@PathVariable("id") Integer id) {
        borrowerService.deleteBorrower(id);
        return "redirect:/borrowers";
    }

    @GetMapping("/borrowers/{id}")
    public String getBorrowerPage(Model model, @PathVariable("id") Integer id) {
        Borrower borrower = borrowerService.getBorrower(id);
        model.addAttribute("borrower", borrower);
        return "borrower";
    }

    @PostMapping("/borrowers/{id}/update")
    public String update(@PathVariable("id") Integer id, BorrowerForm form) {
        borrowerService.updateBorrower(id, form);
        return "redirect:/borrowers/{id}";
    }

    @GetMapping("/borrowers/{id}/feedbacks")
    public String getFeedbacksByBorrower(Model model, @PathVariable("id") Integer id) {
        List<Feedback> feedbacksByBorrower = borrowerService.getFeedbacksByBorrower(id);
        model.addAttribute("feedbacks", feedbacksByBorrower);
        return "feedbacks_of_borrower";
    }
}
