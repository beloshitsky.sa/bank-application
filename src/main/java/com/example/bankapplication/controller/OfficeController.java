package com.example.bankapplication.controller;

import com.example.bankapplication.forms.OfficeForm;
import com.example.bankapplication.models.Manager;
import com.example.bankapplication.models.Office;
import com.example.bankapplication.services.OfficeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class OfficeController {
    private final OfficeService officeService;

    @GetMapping("/offices")
    public String getOfficesPage(Model model) {
        List<Office> offices = officeService.getAllOffices();
        model.addAttribute("offices", offices);
        return "/offices";
    }

    @PostMapping("/offices")
    public String findByAddress(@RequestParam String address, Model model) {
        List<Office> officesByAddress = officeService.findByAddressContainingIgnoreCase(address);
        model.addAttribute("offices", officesByAddress);
        return "/offices";
    }

    @GetMapping("/offices/{id}")
    public String getOfficePage(Model model, @PathVariable("id") Integer id) {
        Office office = officeService.getOffice(id);
        model.addAttribute("office", office);
        return "office";
    }

    @PostMapping("/offices/{id}/update")
    public String updateOffice(@PathVariable("id") Integer id, OfficeForm form,
                               BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "Ошибки в данных!");
            return "redirect:/offices/{id}";
        }
        officeService.updateOffice(id, form);
        return "redirect:/offices/{id}";
    }

    @GetMapping("/offices/{id}/managers")
    public String getAllManagersByOffice(Model model, @PathVariable("id") Integer id) {
        List<Manager> managers = officeService.getAllManagersByOffice(id);
        List<Manager> managersWithoutOffice = officeService.getAllManagersWithoutOffice();
        model.addAttribute("id", id);
        model.addAttribute("managers", managers);
        model.addAttribute("managersWithoutOffice", managersWithoutOffice);
        return "managers_by_office";
    }

    @PostMapping("/offices/{id}/managers")
    public String addManagerToOffice(@PathVariable("id") Integer id,
                                     @RequestParam("managerId") Integer managerId) {
        officeService.addManagerToOffice(id, managerId);
        return "redirect:/offices/" + id + "/managers";
    }
}
