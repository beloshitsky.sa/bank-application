package com.example.bankapplication.controller;

import com.example.bankapplication.forms.FeedbackForm;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.services.FeedbackService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/feedbacks")
public class FeedbackController {

    private final FeedbackService feedbackService;

    @GetMapping
    public String getFeedbacksPage(Model model) {
        List<Feedback> feedbacks = feedbackService.getAllFeedbacks();
        model.addAttribute("feedbacks", feedbacks);
        return "/feedbacks";
    }

    @PostMapping
    public String addFeedback(FeedbackForm form) {
        feedbackService.addFeedback(form);
        return "redirect:/feedbacks";
    }
}
