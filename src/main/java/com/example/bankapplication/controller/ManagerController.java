package com.example.bankapplication.controller;

import com.example.bankapplication.forms.ManagerForm;
import com.example.bankapplication.models.Manager;
import com.example.bankapplication.services.ManagerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ManagerController {

    private final ManagerService managerService;

    @GetMapping("/managers")
    public String getManagersPage(Model model) {
        List<Manager> managers = managerService.getAllManagers();
        model.addAttribute("managers", managers);
        return "/managers";
    }

    @PostMapping("/managers")
    public String addManager(ManagerForm form, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "Ошибки в данных!");
            return "redirect:/managers";
        }
        managerService.addManager(form);
        return "redirect:/managers";
    }

    @PostMapping("/managers/{id}/delete")
    public String deleteManager(@PathVariable("id") Integer id) {
        managerService.deleteManager(id);
        return "redirect:/managers";
    }

    @GetMapping("/managers/{id}")
    public String getManagerPage(Model model, @PathVariable("id") Integer id) {
        Manager manager = managerService.getManager(id);
        model.addAttribute("manager", manager);
        return "manager";
    }

    @PostMapping("/managers/{id}/update")
    public String update(ManagerForm form, @PathVariable("id") Integer id) {
        managerService.updateManager(form, id);
        return "redirect:/managers/{id}";
    }
}
