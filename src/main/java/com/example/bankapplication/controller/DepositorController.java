package com.example.bankapplication.controller;

import com.example.bankapplication.forms.DepositorForm;
import com.example.bankapplication.models.Depositor;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.services.DepositorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class DepositorController {

    private final DepositorService depositorService;

    @GetMapping("/depositors")
    public String getDepositorsPage(Model model) {
        List<Depositor> depositors = depositorService.getAllDepositors();
        model.addAttribute("depositors", depositors);
        return "depositors";
    }

    @GetMapping("/depositors/{id}")
    public String getDepositorPage(Model model, @PathVariable("id") Integer id) {
        Depositor depositor = depositorService.getDepositor(id);
        model.addAttribute("depositor", depositor);
        return "depositor";
    }

    @PostMapping("/depositors")
    public String addDepositor(DepositorForm form, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "Ошибки в данных!");
            return "redirect:/depositors";
        }
        depositorService.addDepositor(form);
        return "redirect:/depositors";
    }

    @PostMapping("/depositors/{id}/delete")
    public String deleteDepositor(@PathVariable("id") Integer id) {
        depositorService.deleteDepositor(id);
        return "redirect:/depositors";
    }

    @PostMapping("/depositors/{id}/update")
    public String update(@PathVariable("id") Integer id, DepositorForm form) {
        depositorService.updateDepositor(id, form);
        return "redirect:/depositors/{id}";
    }

    @GetMapping("/depositors/{id}/feedbacks")
    public String getFeedbacksByDepositor(Model model, @PathVariable("id") Integer id) {
        List<Feedback> feedbacks = depositorService.getFeedbacksByDepositor(id);
        model.addAttribute("feedbacks", feedbacks);
        return "feedbacks_of_depositor";
    }
}
