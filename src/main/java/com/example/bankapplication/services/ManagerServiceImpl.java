package com.example.bankapplication.services;

import com.example.bankapplication.forms.ManagerForm;
import com.example.bankapplication.models.Manager;
import com.example.bankapplication.repositories.ManagersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
public class ManagerServiceImpl implements ManagerService {

    private final ManagersRepository managersRepository;

    @Override
    public Manager addManager(ManagerForm form) {
        Manager manager = Manager.builder()
                .name(form.getName())
                .dateOfBirth(form.getDateOfBirth())
                .email(form.getEmail())
                .seniority(form.getSeniority())
                .build();

        managersRepository.save(manager);
        return manager;
    }

    @Override
    public List<Manager> getAllManagers() {
        return managersRepository.findAll();
    }

    @Override
    public void deleteManager(Integer id) {
        managersRepository.deleteById(id);
    }

    @Override
    public Manager getManager(Integer id) {
        return managersRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Manager with id %s not found", id)));
    }

    @Override
    public Manager updateManager(ManagerForm form, Integer id) {
        Manager managerToUpdate = Manager.builder()
                .id(id)
                .name(form.getName())
                .dateOfBirth(form.getDateOfBirth())
                .email(form.getEmail())
                .seniority(form.getSeniority())
                .build();

        managersRepository.save(managerToUpdate);
        return managerToUpdate;
    }
}
