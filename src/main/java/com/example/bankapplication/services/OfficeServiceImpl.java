package com.example.bankapplication.services;

import com.example.bankapplication.forms.OfficeForm;
import com.example.bankapplication.models.Manager;
import com.example.bankapplication.models.Office;
import com.example.bankapplication.repositories.ManagersRepository;
import com.example.bankapplication.repositories.OfficeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;

@RequiredArgsConstructor
@Component
public class OfficeServiceImpl implements OfficeService {

    private final OfficeRepository officeRepository;

    private final ManagersRepository managersRepository;

    @Override
    public List<Office> getAllOffices() {
        return officeRepository.findAll();
    }

    @Override
    public Office updateOffice(Integer id, OfficeForm form) {
        Office officeToUpdate = officeRepository.getById(id);
        officeToUpdate.setName(form.getName());
        officeToUpdate.setAddress(form.getAddress());
        officeToUpdate.setPhoneNumber(form.getPhoneNumber());
        officeToUpdate.setWeekendWork(form.getWeekendWork());
        officeRepository.save(officeToUpdate);
        return officeToUpdate;
    }

    @Override
    public List<Office> findByAddressContainingIgnoreCase(String address) {
        return officeRepository.findByAddressContainingIgnoreCase(address);
    }

    @Override
    public Office getOffice(Integer id) {
        return officeRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Office with id %s not found", id)));
    }

    @Override
    public List<Manager> getAllManagersByOffice(Integer officeId) {
        return managersRepository.findAllByWorkplace_Id(officeId);
    }

    @Override
    public List<Manager> getAllManagersWithoutOffice() {
        return managersRepository.findAllByWorkplaceIsNull();
    }

    @Override
    public void addManagerToOffice(Integer officeId, Integer managerId) {
        Office office = officeRepository.getById(officeId);
        Manager manager = managersRepository.getById(managerId);
        manager.setWorkplace(office);
        managersRepository.save(manager);
    }
}
