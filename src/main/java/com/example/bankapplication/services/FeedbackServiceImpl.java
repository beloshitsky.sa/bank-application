package com.example.bankapplication.services;

import com.example.bankapplication.forms.FeedbackForm;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.repositories.FeedbackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Override
    public Feedback addFeedback(FeedbackForm form) {
        Feedback feedback = Feedback.builder()
                .content(form.getContent())
                .rate(form.getRate())
                .authorName(form.getAuthorName())
                .build();
        feedbackRepository.save(feedback);
        return feedback;
    }

    @Override
    public List<Feedback> getAllFeedbacks() {
        return feedbackRepository.findAll();
    }
}
