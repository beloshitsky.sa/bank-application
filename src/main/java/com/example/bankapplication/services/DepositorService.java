package com.example.bankapplication.services;

import com.example.bankapplication.forms.DepositorForm;
import com.example.bankapplication.models.Depositor;
import com.example.bankapplication.models.Feedback;

import java.util.List;

public interface DepositorService {
    Depositor addDepositor(DepositorForm form);

    List<Depositor> getAllDepositors();

    void deleteDepositor(Integer id);

    Depositor getDepositor(Integer id);

    Depositor updateDepositor(Integer id, DepositorForm form);

    List<Feedback> getFeedbacksByDepositor(Integer id);
}
