package com.example.bankapplication.services;

import com.example.bankapplication.forms.BorrowerForm;
import com.example.bankapplication.models.Borrower;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.repositories.BorrowersRepository;
import com.example.bankapplication.repositories.FeedbackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
public class BorrowerServiceImpl implements BorrowerService {

    private final BorrowersRepository borrowersRepository;

    private final FeedbackRepository feedbackRepository;

    @Override
    public Borrower addBorrower(BorrowerForm form) {
        Borrower borrower = Borrower.builder()
                .name(form.getName())
                .dateOfBirth(form.getDateOfBirth())
                .phoneNumber(form.getPhoneNumber())
                .sumOfCredit(form.getSumOfCredit())
                .term(form.getTerm())
                .payment(form.getPayment())
                .interestRate(form.getInterestRate())
                .overpayment(form.getOverpayment())
                .build();

        borrowersRepository.save(borrower);
        return borrower;
    }

    @Override
    public List<Borrower> getAllBorrowers() {
        return borrowersRepository.findAll();
    }

    @Override
    public void deleteBorrower(Integer id) {
        borrowersRepository.deleteById(id);
    }

    @Override
    public Borrower getBorrower(Integer id) {
        return borrowersRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Borrower with id %s not found", id)));
    }

    @Override
    public Borrower updateBorrower(Integer id, BorrowerForm form) {
        Borrower borrower = Borrower.builder()
                .id(id)
                .name(form.getName())
                .dateOfBirth(form.getDateOfBirth())
                .phoneNumber(form.getPhoneNumber())
                .sumOfCredit(form.getSumOfCredit())
                .term(form.getTerm())
                .payment(form.getPayment())
                .interestRate(form.getInterestRate())
                .overpayment(form.getOverpayment())
                .build();
        borrowersRepository.save(borrower);
        return borrower;
    }

    @Override
    public List<Feedback> getFeedbacksByBorrower(Integer id) {
        return feedbackRepository.findAllByBorrowerAuthor_Id(id);
    }
}
