package com.example.bankapplication.services;

import com.example.bankapplication.forms.ManagerForm;
import com.example.bankapplication.models.Manager;

import java.util.List;

public interface ManagerService {
    Manager addManager(ManagerForm form);

    List<Manager> getAllManagers();

    void deleteManager(Integer id);

    Manager getManager(Integer id);

    Manager updateManager(ManagerForm form, Integer id);
}
