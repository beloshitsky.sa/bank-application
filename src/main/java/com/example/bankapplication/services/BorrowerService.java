package com.example.bankapplication.services;

import com.example.bankapplication.forms.BorrowerForm;
import com.example.bankapplication.models.Borrower;
import com.example.bankapplication.models.Feedback;

import java.util.List;

public interface BorrowerService {
    Borrower addBorrower(BorrowerForm form);

    List<Borrower> getAllBorrowers();

    void deleteBorrower(Integer id);

    Borrower getBorrower(Integer id);

    Borrower updateBorrower(Integer id, BorrowerForm form);

    List<Feedback> getFeedbacksByBorrower(Integer id);
}
