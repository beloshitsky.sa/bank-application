package com.example.bankapplication.services;

import com.example.bankapplication.forms.DepositorForm;
import com.example.bankapplication.models.Depositor;
import com.example.bankapplication.models.Feedback;
import com.example.bankapplication.repositories.DepositorsRepository;
import com.example.bankapplication.repositories.FeedbackRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static java.lang.String.format;

@RequiredArgsConstructor
@Component
public class DepositorServiceImpl implements DepositorService {

    private final DepositorsRepository depositorsRepository;

    private final FeedbackRepository feedbackRepository;

    @Override
    public Depositor addDepositor(DepositorForm form) {
        Depositor depositor = Depositor.builder()
                .name(form.getName())
                .dateOfBirth(form.getDateOfBirth())
                .phoneNumber(form.getPhoneNumber())
                .depositSum(form.getDepositSum())
                .term(form.getTerm())
                .interestRate(form.getInterestRate())
                .depositIncome(form.getDepositIncome())
                .build();

        depositorsRepository.save(depositor);
        return depositor;
    }

    @Override
    public List<Depositor> getAllDepositors() {
        return depositorsRepository.findAll();
    }

    @Override
    public void deleteDepositor(Integer id) {
        depositorsRepository.deleteById(id);
    }

    @Override
    public Depositor getDepositor(Integer id) {
        return depositorsRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        format("Depositor with id %s not found", id)));
    }

    @Override
    public Depositor updateDepositor(Integer id, DepositorForm form) {
        Depositor depositor = Depositor.builder()
                .id(id)
                .name(form.getName())
                .dateOfBirth(form.getDateOfBirth())
                .phoneNumber(form.getPhoneNumber())
                .depositSum(form.getDepositSum())
                .term(form.getTerm())
                .interestRate(form.getInterestRate())
                .depositIncome(form.getDepositIncome())
                .build();

        depositorsRepository.save(depositor);
        return depositor;
    }

    @Override
    public List<Feedback> getFeedbacksByDepositor(Integer id) {
        return feedbackRepository.findAllByDepositorAuthor_id(id);
    }
}
