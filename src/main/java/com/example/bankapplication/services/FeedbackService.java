package com.example.bankapplication.services;

import com.example.bankapplication.forms.FeedbackForm;
import com.example.bankapplication.models.Feedback;

import java.util.List;

public interface FeedbackService {
    Feedback addFeedback(FeedbackForm form);

    List<Feedback> getAllFeedbacks();
}
