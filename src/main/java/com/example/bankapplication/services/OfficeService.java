package com.example.bankapplication.services;

import com.example.bankapplication.forms.OfficeForm;
import com.example.bankapplication.models.Manager;
import com.example.bankapplication.models.Office;

import java.util.List;

public interface OfficeService {
    List<Office> getAllOffices();

    Office updateOffice(Integer id, OfficeForm form);

    List<Office> findByAddressContainingIgnoreCase(String address);

    Office getOffice(Integer id);

    List<Manager> getAllManagersByOffice(Integer officeId);

    List<Manager> getAllManagersWithoutOffice();

    void addManagerToOffice(Integer officeId, Integer managerId);
}
