package com.example.bankapplication.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Borrower {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String dateOfBirth;
    private String phoneNumber;
    @Column(columnDefinition = "integer default 10000")
    private double sumOfCredit;
    private Integer term;
    private double payment;
    private double interestRate;
    private double overpayment;

    @OneToMany(mappedBy = "borrowerAuthor")
    private List<Feedback> feedbacksByBorrower;
}
