package com.example.bankapplication.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Depositor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String dateOfBirth;
    private String phoneNumber;
    private double depositSum;
    private Integer term;
    private double interestRate;
    private double depositIncome;

    @OneToMany(mappedBy = "depositorAuthor")
    private List<Feedback> feedbacks;
}
