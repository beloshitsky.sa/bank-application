package com.example.bankapplication.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String content;
    private Integer rate;
    private String authorName;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Depositor depositorAuthor;

    @ManyToOne
    @JoinColumn(name = "borrower_id")
    private Borrower borrowerAuthor;
}
