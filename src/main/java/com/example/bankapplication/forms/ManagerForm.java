package com.example.bankapplication.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ManagerForm {
    @NotEmpty
    private String name;

    @NotEmpty
    private String dateOfBirth;

    @NotEmpty
    private String email;

    @Min(value = 0)
    private Integer seniority;

    public Integer getSeniority() {
        if (seniority == null) {
            setSeniority(0);
        }
        return seniority;
    }
}
