package com.example.bankapplication.forms;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
public class OfficeForm {
    @NotEmpty
    private String name;

    @NotEmpty
    private String address;

    @NotEmpty
    private String phoneNumber;

    @NotEmpty
    @Size(max = 1)
    @Pattern(regexp = "^[yn]{1,1}$")
    private char weekendWork;
}
