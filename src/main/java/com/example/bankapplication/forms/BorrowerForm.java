package com.example.bankapplication.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BorrowerForm {

    @NotEmpty
    private String name;

    @NotEmpty
    private String dateOfBirth;

    @NotEmpty
    private String phoneNumber;

    @NotEmpty
    @DecimalMin(value = "0")
    private double sumOfCredit;

    @Min(value = 0)
    private Integer term;

    private double payment;

    private double interestRate;

    private double overpayment;

    public double getInterestRate() {
        if (sumOfCredit >= 0 && sumOfCredit <= 500000) setInterestRate(13.5);
        if (sumOfCredit >= 500001 && sumOfCredit <= 2000000) setInterestRate(11);
        if (sumOfCredit >= 2000001) setInterestRate(7.5);
        if (interestRate <= 7.5) setInterestRate(7.5);
        return interestRate;
    }

    public double getPayment() {
        double monthInterestRate = getInterestRate() / 100 / 12;
        double payment = sumOfCredit * (monthInterestRate + (monthInterestRate / (Math.pow(1 + monthInterestRate, term) - 1)));
        setPayment(payment);
        payment = Math.round(payment * 100d) / 100d;
        return payment;
    }

    public double getOverpayment() {
        double overpayment = payment * term - sumOfCredit;
        setOverpayment(overpayment);
        overpayment = Math.round(overpayment * 100d) / 100d;
        return overpayment;
    }
}
