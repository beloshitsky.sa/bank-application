package com.example.bankapplication.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DepositorForm {
    @NotEmpty
    private String name;

    @NotEmpty
    private String dateOfBirth;

    @NotEmpty
    private String phoneNumber;

    @DecimalMin(value = "0")
    private double depositSum;

    @Min(value = 0)
    private Integer term;

    private double interestRate;

    private double depositIncome;

    public double getInterestRate() {
        if (depositSum <= 1000) setInterestRate(0.01);
        if (depositSum > 1000 && depositSum <= 100000) setInterestRate(3);
        if (depositSum >= 100001 && depositSum <= 500000) setInterestRate(5.5);
        if (depositSum >= 500001) setInterestRate(7.5);
        return interestRate;
    }

    public double getDepositIncome() {
        double monthInterestRate = getInterestRate() / 100 / 12;
        double depositIncome = depositSum * Math.pow((1 + monthInterestRate), term) - depositSum;
        setDepositIncome(depositIncome);
        depositIncome = Math.round(depositIncome * 100d) / 100d;
        return depositIncome;
    }
}
