package com.example.bankapplication.forms;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FeedbackForm {
    String content;
    Integer rate;
    String authorName;

    public Integer getRate() {
        if (rate > 5) setRate(5);
        if (rate < 0) setRate(0);
        return rate;
    }
}
