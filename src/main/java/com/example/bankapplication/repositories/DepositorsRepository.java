package com.example.bankapplication.repositories;

import com.example.bankapplication.models.Depositor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositorsRepository extends JpaRepository<Depositor, Integer> {
}
