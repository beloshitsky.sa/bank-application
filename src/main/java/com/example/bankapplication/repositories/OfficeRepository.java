package com.example.bankapplication.repositories;

import com.example.bankapplication.models.Office;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfficeRepository extends JpaRepository<Office, Integer> {
    List<Office> findByAddressContainingIgnoreCase(String address);
}
