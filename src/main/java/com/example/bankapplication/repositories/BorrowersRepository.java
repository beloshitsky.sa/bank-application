package com.example.bankapplication.repositories;

import com.example.bankapplication.models.Borrower;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BorrowersRepository extends JpaRepository<Borrower, Integer> {
}
