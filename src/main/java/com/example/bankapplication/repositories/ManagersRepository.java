package com.example.bankapplication.repositories;

import com.example.bankapplication.models.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ManagersRepository extends JpaRepository<Manager, Integer> {
    List<Manager> findAllByWorkplace_Id(Integer id);

    List<Manager> findAllByWorkplaceIsNull();
}
