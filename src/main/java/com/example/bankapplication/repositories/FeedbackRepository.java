package com.example.bankapplication.repositories;

import com.example.bankapplication.models.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {
    List<Feedback> findAllByDepositorAuthor_id(Integer id);

    List<Feedback> findAllByBorrowerAuthor_Id(Integer id);
}
