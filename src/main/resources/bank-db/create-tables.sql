create table depositor
(
    id             serial primary key,
    name           varchar(255) not null,
    date_of_birth  varchar(255) not null,
    phone_number   varchar(255) not null,
    deposit_sum    double precision,
    term           Integer,
    interest_rate  double precision,
    deposit_income double precision
);

insert into depositor (name, date_of_birth, phone_number, deposit_sum, term, interest_rate, deposit_income)
values ('Depositor 1', '01-01-2000', '+79991234567', 0, 0, 0, 0),
       ('Depositor 2', '01-02-2000', '+79881234567', 0, 0, 0, 0),
       ('Depositor 3', '01-03-2000', '+79891234567', 0, 0, 0, 0),
       ('Depositor 4', '01-04-2000', '+79791234567', 0, 0, 0, 0),
       ('Depositor 5', '01-05-2000', '+79001234567', 0, 0, 0, 0);

create table borrower
(
    id            serial primary key,
    name          varchar(255) not null,
    date_of_birth varchar(255) not null,
    phone_number  varchar(255) not null,
    sum_of_credit double precision,
    term          Integer,
    payment       double precision,
    interest_rate double precision,
    overpayment   double precision
);

insert into borrower (name, date_of_birth, phone_number, sum_of_credit, term, payment, interest_rate, overpayment)
values ('Borrower 1', '10-01-2000', '+79001234567', 0, 0, 0, 0, 100),
       ('Borrower 2', '10-01-2001', '+79011234567', 0, 0, 0, 0, 100),
       ('Borrower 3', '10-01-2003', '+79201234567', 0, 0, 0, 0, 100),
       ('Borrower 4', '10-01-1999', '+79301234567', 0, 0, 0, 0, 100),
       ('Borrower 5', '10-01-1998', '+79031234567', 0, 0, 0, 0, 100);

create table feedback
(
    id          serial primary key,
    content     varchar(255),
    rate        Integer,
    name        varchar(255),
    author_id   integer,
    borrower_id integer,
    foreign key (author_id) references depositor (id) on delete set default,
    foreign key (borrower_id) references borrower (id) on delete set default
);

insert into feedback (content, rate, author_id, borrower_id)
values ('Это моё субъективное мнение', 0, 1, null),
       ('Это моё субъективное мнение', 1, 2, null),
       ('Это моё субъективное мнение', 3, 2, null),
       ('Это моё субъективное мнение', 4, 4, null),
       ('Это моё субъективное мнение', 5, 5, null),
       ('Это моё субъективное мнение', 1, null, 2),
       ('Это моё субъективное мнение', 5, null, 3),
       ('Это моё субъективное мнение', 3, null, 2),
       ('Это моё субъективное мнение', 4, null, 1),
       ('Это моё субъективное мнение', 5, null, 1);

create table office
(
    id           serial primary key,
    name         varchar(255) not null,
    address      varchar(255) not null,
    phone_number varchar(255) not null,
    weekend_work char
);

insert into office (name, address, phone_number, weekend_work)
values ('Office 1', 'Большая Покровская 50', '88005553535', 'y'),
       ('Office 2', 'Большая Покровская 10', '88005553535', 'n'),
       ('Office 3', 'Ленина 100', '88005553535', 'n'),
       ('Office 4', 'Ленина 10', '88005553535', 'n'),
       ('Office 5', 'Университетская 1', '88005553535', 'y'),
       ('Office 6', 'Безымянная 20', '88005553535', 'y');

create table manager
(
    id            serial primary key,
    name          varchar(255) not null,
    date_of_birth varchar(255) not null,
    email         varchar(255) not null,
    seniority     Integer,
    office_id     Integer,
    foreign key (office_id) references office (id) on delete set default
);

insert into manager (name, date_of_birth, email, seniority, office_id)
values ('Manager 1', '15-02-1992', 'manager1@bank.com', 5, 1),
       ('Manager 2', '15-02-2000', 'manager2@bank.com', 0, null),
       ('Manager 3', '15-02-2002', 'manager3@bank.com', 0, null),
       ('Manager 4', '15-02-1995', 'manager4@bank.com', 0, null),
       ('Manager 5', '15-02-1978', 'manager5@bank.com', 10, 2),
       ('Manager 6', '15-02-1988', 'manager6@bank.com', 7, 3),
       ('Manager 7', '15-02-1980', 'manager7@bank.com', 15, 2),
       ('Manager 8', '15-02-1985', 'manager8@bank.com', 5, 4),
       ('Manager 9', '15-02-1993', 'manager9@bank.com', 5, 4),
       ('Manager 10', '15-02-1983', 'manager10@bank.com', 1, 1);
